function register(render) {
  (function callback() {
    render();
    requestAnimationFrame(callback);
  })();
}

export default register;
