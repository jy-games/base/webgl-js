import register from './base';

import './style.scss';

let canvas, context;
let lt;

function rainbow(h) {
  var r, g, b, i, f, q;
  i = Math.floor(h * 6);
  f = h * 6 - i;
  q = 1 - f;
  switch (i % 6) {
      case 0: r = 1, g = f, b = 0; break;
      case 1: r = q, g = 1, b = 0; break;
      case 2: r = 0, g = 1, b = f; break;
      case 3: r = 0, g = q, b = 1; break;
      case 4: r = f, g = 0, b = 1; break;
      case 5: r = 1, g = 0, b = q; break;
  }
  return [
      Math.round(r * 255),
      Math.round(g * 255),
      Math.round(b * 255),
  ].join();
}

function render() {
  let now = Date.now();
  let dt = now - lt;

  const len = 3000;
  context.beginPath();
  context.rect(0, 0, canvas.clientWidth, canvas.clientHeight);
  context.fillStyle = `rgb(${rainbow(now % len / len)})`;
  context.fill();
  console.log(dt);

  lt = now;
}

function init() {
  lt = Date.now();
  canvas = document.getElementById('root');
  context = canvas.getContext("2d");
  register(render);
}

window.addEventListener('load', init);
